package xyz.urelay.test.pages;

import org.openqa.selenium.WebDriver;

public class LoadsMapPage extends AbstractPage{

    public LoadsMapPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getValidTitle() {
        return "Loads Map";
    }

}
