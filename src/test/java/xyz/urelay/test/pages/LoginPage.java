package xyz.urelay.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends AbstractPage{

    private By loginContentPopup = By.className("login_content");

    private By usernameLocator = By.id("user-email");
    private By passwordLocator = By.id("user-password");
    private By loginButtonLocator = By.cssSelector("button.btn.login-up-button");
    private By alert = By.cssSelector(".alert.alert-danger");
    private By forgotPasswordLink = By.ByLinkText.linkText("Forgot Password?");
    private By signUpButton = By.className("btn sign-in-button");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String getValidTitle() {
        return "Login";
    }

    public void typeUsername(String username) {
        driver.findElement(usernameLocator).sendKeys(username);
    }

    public void typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);
    }

    public LoadsMapPage submitLogin() {
        driver.findElement(loginButtonLocator).submit();
        LoadsMapPage newPage = new LoadsMapPage(driver);
        return newPage;
    }

    public LoginPage submitLoginExpectingFailure() {
        driver.findElement(loginButtonLocator).submit();
        return new LoginPage(driver);
    }

    public String getAlertMessage() {
        WebElement element = driver.findElement(alert);
        String message = element.getText();
        return message.substring(2);
    }


}