package xyz.urelay.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by N_PC on 15.08.2017.
 */
public class TruckPostingPage  extends AbstractPage{

    @FindBy(css = "[ui-sref = 'marketplaceCreate']")
    private WebElement createPost;

    public TruckPostingPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getCreatePost () {
        return createPost;
    }



    @Override
    protected String getValidTitle() {
        return "Posting";
    }

}

