package xyz.urelay.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class MainPage extends AbstractPage {

    @FindBy(xpath = "//a[contains(text(),'sign up')]")
    private WebElement signUpButton;

    @FindBy(name = "loginForm")
    private WebElement loginForm;

    @FindBy(css = "[ng-click]='showLoginPopUp()'")
    private WebElement loginButton;

    @FindBy(css = "[ng-click='!steps[0].inProgress && stepOne(user)']")
    private WebElement continue1;

    @FindBy(css = "[ng-click='!steps[1].inProgress && stepTwo(company)']")
    private WebElement continue2;

    @FindBy(css = "[ng-click='confirm(previewForm);']")
    private WebElement createAccount;

    @FindBy(name = "userForm")
    private WebElement step1;

    @FindBy(name = "firstName")
    private WebElement firstName;

    @FindBy(name = "lastName")
    private WebElement lastName;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "phone")
    private WebElement phone;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(name = "passwordVerification")
    private WebElement confirmPassword;

    @FindBy(id = "agree")
    private WebElement agree;

    @FindBy(name = "companyForm")
    private WebElement step2;

    @FindBy(id = "dropdownMenu1")
    private WebElement dropdownMenu;

    @FindBy(name = "name")
    private WebElement companyName;

    @FindBy(name = "mc")
    private WebElement mcNumber;

    @FindBy(name = "previewForm")
    private WebElement step3;

    @FindBy(id = "correct")
    private WebElement previewAgree;


    @FindBy(css = "[ng-show='userForm.firstName.$error.required']")
    private WebElement validationFirstName;


    @FindBy(css = "[ng-show='userForm.lastName.$error.required']")
    private WebElement validationLastName;


    @FindBy(css = "[ng-show='serForm.email.$error.required']")
    private WebElement validationEmail;


    @FindBy(css = "[ng-show='userForm.password.$error.required']")
    private WebElement validationPassword;


    @FindBy(css ="[ng-show='userForm.passwordVerification.$error.required']")
    private WebElement validationConPassword;


    @FindBy(css = "span[id = 'text-72']")
    private WebElement validationAgreement;



    public void clickloginButton() {
        click(loginButton);
    }

    public void clickAgree() {
        click(previewAgree);
    }

    public void typemcNumber(int mc) {
        mcNumber.sendKeys(String.valueOf(mc));
    }

    public void clickDropdown() {
        click(dropdownMenu);
    }

    public void typeCompanyName(String companyName) {
        this.companyName.sendKeys(companyName);
    }

    public void selectRole(int role) {
        switch (role) {
            case 1:
                driver.findElement(By.xpath("//a[contains(.,'Carrier')]")).click();
                break;
            case 2:
                driver.findElement(By.xpath("//a[contains(.,'Broker')]")).click();
                break;
            case 3:
                driver.findElement(By.xpath("//a[contains(.,'Shipper')]")).click();
                break;
        }

    }

    public MainPage(WebDriver driver) {
        super(driver);
    }

    protected String getValidTitle() {
        return "RigGoh - connecting carriers, brokers and shippers online";
    }

    public void submitSignUp() {
        click(signUpButton);
    }

    public void clickContinue1() {
        click(continue1);
    }

    public void clickContinue2() {
        click(continue2);
    }

    public void clickCreateAccount() {
        click(createAccount);
    }


   public boolean isValidationStep1Visible() {
        return validationFirstName.isDisplayed() && validationLastName.isDisplayed() && validationEmail.isDisplayed() && validationPassword.isDisplayed() && validationConPassword.isDisplayed() && validationAgreement.isDisplayed();
    }

    public boolean isLoginFormVisible() {
        return loginForm != null && loginForm.isDisplayed();
    }

    public boolean isStep2Visible() {
        return step2 != null && step2.isDisplayed();
    }

    public boolean isStep1Visible() {
        return step1 != null && step1.isDisplayed();
    }

    public boolean isStep3Visible() {
        return step3 != null && step3.isDisplayed();
    }

    public void fillForm(String firstName, String lastName, String email, String phone, String password, String confirmPassword) {
        this.firstName.sendKeys(firstName);
        this.lastName.sendKeys(lastName);
        this.email.sendKeys(email);
        this.phone.sendKeys(phone);
        this.password.sendKeys(password);
        this.confirmPassword.sendKeys(confirmPassword);
        click(agree);
    }


}
