package xyz.urelay.test.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class AbstractPage {

    protected final WebDriver driver;

    AbstractPage(WebDriver driver) {
        this.driver = driver;

    }


    protected abstract String getValidTitle();

    protected void checkPageLoadsMap() throws IllegalStateException {
        if (!getValidTitle().equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the LoadsMap page. The page title is "+driver.getTitle());
        }
    }

    protected void checkPageTruckPosting() throws IllegalStateException {
        if (!getValidTitle().equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the Truck Posting page. The page title is "+driver.getTitle());
        }
    }

    protected void click(WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
    }
}
