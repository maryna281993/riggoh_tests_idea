package xyz.urelay.test;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import xyz.urelay.test.pages.MainPage;
import xyz.urelay.test.utils.TestUtils;

public class SignUpTest extends ChromeWebDriverTest {

    private static final int VALIDATION_DELAY = 250;
    private static final int TAB_DELAY = 2000;

    @Test
    public void succesTest() throws InterruptedException {
        driver.get("https://dev2.urelay.xyz/");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);

        mainPage.submitSignUp();
        Assert.assertTrue(mainPage.isStep1Visible());
        mainPage.fillForm("Vasya" + TestUtils.generateNumber(1000), "Pupkin" + TestUtils.generateNumber(1000), "pupkin" + TestUtils.generateNumber(1000) + "@mail.ru", "093480" + TestUtils.generateNumber(1000), "123456789", "123456789");
        Thread.sleep(VALIDATION_DELAY);
        mainPage.clickContinue1();
        Thread.sleep(TAB_DELAY);
        Assert.assertTrue(mainPage.isStep2Visible());
        mainPage.clickDropdown();
        mainPage.selectRole(2);
        mainPage.typeCompanyName("Jameson" + TestUtils.generateNumber(1000));
        mainPage.typemcNumber(1);
        Thread.sleep(VALIDATION_DELAY);
        mainPage.clickContinue2();
        Thread.sleep(TAB_DELAY);
        Assert.assertTrue(mainPage.isStep3Visible());
        mainPage.clickAgree();
        Thread.sleep(VALIDATION_DELAY);
        mainPage.clickCreateAccount();


    }

  /*@Test
    public void emptyFields() {
        driver.get("https://dev2.urelay.xyz/");

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);

        mainPage.submitSignUp();
        Assert.assertTrue(mainPage.isStep1Visible());
        mainPage.clickContinue1();
        Assert.assertTrue(mainPage.isStep1Visible());
        Assert.assertTrue(mainPage.isValidationStep1Visible());
        Assert.assertFalse(mainPage.isStep2Visible());
    }*/
}


