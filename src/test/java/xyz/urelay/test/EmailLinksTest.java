package xyz.urelay.test;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import xyz.urelay.test.pages.LoginPage;
import xyz.urelay.test.pages.TruckPostingPage;

import javax.sound.midi.Track;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Email link test for no loggined and loggined user
 * Created by N_PC on 15.08.2017.
 */

public class EmailLinksTest extends ChromeWebDriverTest {
    @Test
        public void linkTest() throws InterruptedException {

        driver.get("https://riggoh.com/app/spa/marketplace/posting");
        /**Initialization Login Page
         * @see LoginPage */
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        /**Initialization Login Page
         * @see TruckPostingPage */
        TruckPostingPage truckPostingPage = PageFactory.initElements(driver, TruckPostingPage.class);
        /**Comparison of titles*/
        Assert.assertTrue("Login".equals(driver.getTitle()));
        /** Type login
         * @see LoginPage#typeUsername(String)
         * @param email
         */
        loginPage.typeUsername("maryna28.1993+1107@gmail.com");
        /** Type password
         * @see LoginPage#typePassword(String) (String)
         * @param password
         */
        loginPage.typePassword("8096686");
        /** @see LoginPage#submitLogin() */
        loginPage.submitLogin();

        /** Waiting for element preset */
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement waitPostButton1 = wait.until(ExpectedConditions.visibilityOf(truckPostingPage.getCreatePost()));
        /**Comparison of titles*/
        Assert.assertTrue("Posting".equals(driver.getTitle()));
        /** Loggined user go to link */
        driver.get("https://riggoh.com/app/spa/marketplace/posting");
        /** Waiting for element preset */
        WebElement waitPostButton2 = wait.until(ExpectedConditions.visibilityOf(truckPostingPage.getCreatePost()));
        /**Comparison of titles*/
        Assert.assertTrue("Posting".equals(driver.getTitle()));
    }
}

