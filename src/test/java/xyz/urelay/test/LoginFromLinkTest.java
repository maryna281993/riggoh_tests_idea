package xyz.urelay.test;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import xyz.urelay.test.pages.LoadsMapPage;
import xyz.urelay.test.pages.LoginPage;

public class LoginFromLinkTest extends ChromeWebDriverTest {

    @Test
    public void failureLoginTest() {
        driver.get("https://dev2.urelay.xyz/app/login");

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.typeUsername("Vasya");
        loginPage.typePassword("Password");
        loginPage.submitLoginExpectingFailure();
        String message = loginPage.getAlertMessage();
        Assert.assertEquals(message, "failed login");
    }

    @Test
    public void successLoginTest() {
        driver.get("https://dev2.urelay.xyz/app/login");

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.typeUsername("ureley.test@gmail.com");
        loginPage.typePassword("121212");
        LoadsMapPage loadsMapPage = loginPage.submitLogin();
    }

}
