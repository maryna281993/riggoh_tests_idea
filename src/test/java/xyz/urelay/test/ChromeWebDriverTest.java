package xyz.urelay.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class ChromeWebDriverTest {

    protected WebDriver driver;

    @BeforeClass
    public void beforeClass() {
//        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //http://chromedriver.storage.googleapis.com/index.html?path=2.30/
//        driver = new ChromeDriver();
        System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe"); //https://github.com/mozilla/geckodriver/releases
        driver = new FirefoxDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }
}
